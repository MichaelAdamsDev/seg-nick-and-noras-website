/**
 * PENN Stack Builder/Update Service
 * DO NOT EDIT:
 */

//  Check required Environment Variables
['BITBUCKET_REPO_SLUG','BITBUCKET_COMMIT'].forEach((v)=>{
    if(process.env.hasOwnProperty(v))
        return ('Missing Environment Variable: ['+v+'];');
});

//  Pull libraries
const fs = require('fs')
const path = require('path')
const node_ssh = require('node-ssh')
const yaml = require('js-yaml')
const ssh = new node_ssh()

//  Load Config
const config = yaml.safeLoad(fs.readFileSync('./config.yml', 'utf8'));

const APP_DIR = '~/services/' + process.env.BITBUCKET_REPO_SLUG;

//  Setup SSH Connection
ssh.connect({
    host: config.ssh.host,
    username: config.ssh.user,
    privateKey: '~/.ssh/id_rsa'
})
.then(() => {

    ssh.exec('git pull', ['--json'], {
        cwd: APP_DIR,
        stream: 'stdout',
        options: { pty: true }
    }).then( (git_result) => {

        ssh.exec('pm2 restart ' + process.env.BITBUCKET_BRANCH, ['--json'], {
            cwd: APP_DIR,
            stream: 'stdout',
            options: { pty: true }
        }).then((service_result) => {

            return true;

        })

    })

})