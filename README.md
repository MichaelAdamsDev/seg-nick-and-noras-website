# Initial Setup #

Do not clone this repo, `Fork` it.

*	Boot up a new VPS from Vultr using Ubuntu 16+ ( Any LTS Variant )
*	ssh into the VPS ( ssh -v root@IP_ADDRESS )
*	cd ~ ( if not already )
*	mkdir services && cd services
*	sudo apt-get install git redis
*	git clone {REPO ADDRESS}
*	adjust config.yml accordingly

#	Express JS Boilerplate

	@	Assets ( Such as CSS,JS,Images ) are located in the assets folder
		and should be accessed via "/assets/*" in your templates.

	@	Routing is auto populated based on the "routes" folder structure.
		-	e.g. to access "/some-category/some-page" you would create "/routes/some-category/some-page.js"

	@	Templating uses "Pug" and should be setup inside a route file adding "res.render('template-name',{DATA})"
		-	template files are located in "views"

#	Example Database Usage

	-	Define Datastore for Each Typeset ( e.g. Table )
	-	"autoload: true" for data needed on page templates
	-	const pages = new Datastore({ filename: options.Datastore+'pages.db', autoload: true });
		const users = new Datastore({ filename: options.Datastore+'users.db' });

		then...

		db.users.loadDatabase();

	-	Insert Record
	-	users.insert({ name:'record name' } or Array[Object], (err, doc) => {  
			
			console.log('Inserted', doc.name, 'with ID', doc._id);

		});

	-	Find Record
	-	users.findOne({ name: 'record name' }, (err, doc) => {  
			
			console.log('Found user:', doc.name);

		});

	-	Sort Results
	-	users.sort({name: 1}).exec((err, doc) => {  
			
			docs.forEach((d) => {

				console.log('Found user:', d.name);

			});

		});

	-	Remove
	-	users.remove({ name: { $regex: /^RecordNameString/ } }, (err, numDeleted) => {  
		
			console.log('Deleted', numDeleted, 'user(s)');
		
		});

		or for multiple

	-	users.remove({}, { multi: true }, function(err, numDeleted) {  
		
			console.log('Deleted', numDeleted, 'user(s)');
		
		});

#	Example Message Sending.

This is recommended to use with "Enquiry" type forms, not "user" emails.

	const msg = {
		to: 'test@example.com',
		from: 'test@example.com',
		subject: 'Sending with SendGrid is Fun',
		text: 'and easy to do anywhere, even with Node.js',
		html: '<strong>and easy to do anywhere, even with Node.js</strong>',
	};

	mail.send(msg);

# Setting up Staging Server

replace "my-repo-id" with repo url id ( i.e. "mjolner-website" etc, adjust the ".com" to the service url  i.e. mjolner.com.au )

```cd ~/services```
```git clone -b dev https://bitbucket.org/collectiveus/my-repo-id.git staging.my-repo-id.com```
```cd staging.my-repo-id.com```
```npm install```

change "port" to 3838 in config.yml
```pm2 start index.js --name staging```
```pm2 save```

create staging nginx entry
```sudo nano /etc/nginx/sites-available/staging```
Paste below into editor
```
server {

        listen 80;

        server_name staging.my-repo-id.com;

        location / {
                proxy_pass http://localhost:3838;
                proxy_http_version 1.1;
                proxy_set_header Upgrade $http_upgrade;
                proxy_set_header Connection 'upgrade';
                proxy_set_header Host $host;
                proxy_cache_bypass $http_upgrade;
        }
}
```
press ```ctrl+x``` to save and exit editor.

create symlink into "sites-enabled"
```sudo ln -s /etc/nginx/sites-available/staging /etc/nginx/sites-enabled/staging```
restart nginx
```sudo service nginx restart```