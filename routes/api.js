const express = require('express')

const router = express.Router()
const bodyParser = require('body-parser');
const querystring = require('querystring');

// const options = require('config.yml');
const FB = require('fb');
const redisService = require('redis');
const options = require('config-yml');
const redis = redisService.createClient({ prefix: options.RedisPrefix });

const multer = require('multer')

const upload = multer({ dest: 'uploads/' })

const mail = require('@sendgrid/mail');

const fs = require('fs');

const request = require('request');

router.use(bodyParser.urlencoded({ extended: true }));

router.get('/events', (req, res) => {
    eventsResponder(req, res, {
        pageId: "966259810198809",
        scope: this
    });
});

function eventsResponder(req, res, options) {
    var _this = options.scope;
    var config = {
        facebookAppDetails: { // https://developers.facebook.com/
            appId: "1792394730790637",
            appSecret: "7fe373d95d851c6e65e34afa1f2409bc", // Application Secret
            pageId: options.pageId, // https://facebook.com/{TestBar2017} (where {} is the ID / Tag)
            autoExpireTime: 86400, // Time facebook data expires, in secconds (86400 = 24 hours)
            apiVersion: "v2.10", //Graph Api Version
            redisCacheKeys: { // Keys to use when storing data into redis
                accessToken: "fb_accessToken",
                dataCache: "fb_dataCache_" + options.pageId
            },
            dataFields: [
                    "name",
                    "place",
                    "start_time",
                    "end_time",
                    "description",
                    "cover",
                    "category",
                    "ticket_url",
                    "timezone",
                    "type",
                    "interested_count",
                    "attending_count"
                ] //Extra fields to return ( https://developers.facebook.com/docs/graph-api/reference/event/ )
        },
        generalDetails: {
            // ~~~~~~ EDIT START ~~~~~~
            autoExpireTime: 86400, // Time normalized data expires, in secconds (86400 = 24 hours)
            // ~~~~~~  EDIT END  ~~~~~~        
            allowRemoteCacheClear: true, // allow remote access to clear cache
            redisCacheKeys: { // Keys to use when storing data into redis
                dataCache: "gn_dataCache_" + options.pageId
            },
        }
    }

    var EventClient = require("../modules/node-events-module");
    var eventClient = new EventClient.EventClient(config);
    eventClient.getEvents(function(err, data) {
        if (err) {
            res.status(500);
            res.json({
                error: err
            });
        }
        res.status(200);
        res.json(data);
    });
};

router
    .post('/parramatta/join-our-team', upload.single('fileresume'), (req, res) => {

        req.body = JSON.parse(JSON.stringify(req.body));

        let message = '';

        let errors = [];

        if (req.body['contact_via_fax']) {
            return res.end();
        }

        ['fullname', 'email', 'phone', 'perm_resident', 'where_would_you_like_to_work', 'message', 'location'].forEach((row) => {

            if (!req.body.hasOwnProperty(row))
                errors.push('Missing Field ' + row);
            else
                message += ("<h5>" + row + ": " + req.body[row] + "\n");

        });

        const msg = {
            from: 'parramatta@nickandnoras.com.au',
            subject: 'New "Join Our Team" Enquiry Parramatta',
            html: message
        };

        try {
            if (req.file) {
                const data = fs.readFileSync(req.file.path);
                data.attachments = [{
                    type: req.file.mimetype,
                    disposition: 'attachment',
                    filename: req.file.originalname,
                    content: data.toString('base64'),
                }]
            }

            msg.to = "parramatta@nickandnoras.com.au"

            mail.send(msg);
        } catch (error) {
            console.log('error in mail sending', error);
        }


        res.redirect('/parramatta');

    });

router
    .post('/melbourne/join-our-team', upload.single('fileresume'), (req, res) => {

        req.body = JSON.parse(JSON.stringify(req.body));

        let message = '';

        let errors = [];

        if (req.body['contact_via_fax']) {
            return res.end();
        }

        ['fullname', 'email', 'phone', 'perm_resident', 'where_would_you_like_to_work', 'message', 'location'].forEach((row) => {

            if (!req.body.hasOwnProperty(row))
                errors.push('Missing Field ' + row);
            else
                message += ("<h5>" + row + ": " + req.body[row] + "\n");

        });

        const msg = {
            from: 'melbourne@nickandnoras.com.au',
            subject: 'New "Join Our Team" Enquiry Melbourne',
            html: message
        };

        try {
            if (req.file) {
                const data = fs.readFileSync(req.file.path);
                data.attachments = [{
                    type: req.file.mimetype,
                    disposition: 'attachment',
                    filename: req.file.originalname,
                    content: data.toString('base64'),
                }]
            }

            msg.to = "melbourne@nickandnoras.com.au"

            mail.send(msg);
        } catch (error) {
            console.log('error in mail sending', error);
        }


        res.redirect('/melbourne');

    });

router
    .post('/recaptcha', (req, res) => {
        var parsedBody = JSON.parse(req.body.formdata);

        request.post('https://www.google.com/recaptcha/api/siteverify', {
            form: {
                secret: '6LefHZEUAAAAAMLMZA64EGyWbDcVpnIiztMfOMkX',
                response: req.body.recaptcha_response
            }
        }, (error, incomingMessage, response) => {
            response = JSON.parse(response)
            if (error === null && response.success) {
                let mglFormData = {
                    'Time[]': [],
                    'CustomCheckboxes[]': [],
                    'DateOfYourEvent': [],
                    'mgldaterealval__DateOfYourEvent': []
                };

                parsedBody.forEach((field) => {
                    console.log('[api.js] field.name:', field.name);
                    if (field.name == 'Time[]') {
                        mglFormData['Time[]'].push(field.value);
                    } else if (field.name == 'CustomCheckboxes[]') {
                        mglFormData['CustomCheckboxes[]'].push(field.value);
                    } else if (field.name == 'DateOfYourEvent' || field.name == 'mgldaterealval__DateOfYourEvent') {
                        mglFormData[field.name] = field.value;
                    } else {
                        mglFormData[field.name] = field.value
                    }
                });

                let options = {
                    method: 'POST',
                    url: 'https://myguestlist.com/mgl/formreceiver.php',
                    // url: 'http://localhost:3838/api/test',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    form: querystring.stringify(mglFormData)
                };

                request(options, (error, response, body) => {
                    if (error) {
                        console.log('[api.js] error:', error);
                    }
                    res.status(200).end();
                });
            }
        });
    });

router
    .post('/test', (req, res) => {
        res.status(200).end();
    });

module.exports = router