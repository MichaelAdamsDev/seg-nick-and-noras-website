const express = require('express')

const router = express.Router()

// optional page cache ( recommended unless strictly not needed )
const cache = require('express-redis-cache')()

// Show thank you message on form submit
router.use((req, res, next) => {
	if (req.query.formSubmitted) {
		res.render('thank-you', {
			Page: {
				Title: "Thank You",
				IsNonIndex: true,
			},
			Meta: {
				Title: "Thank You"
			}
		});
		console.log(req.query);
	} else {
		next();
	}
});

// define the home page route
router
	//	remove cache.route() to disable caching.
	//.get('/', (req, res) => {
	.get('/',  (req, res) => {
		res.render('index', {
			Page: {
				Title: 'Home'
			}
		})
	})

// Parramatta page
router
	//	remove cache.route() to disable caching.
	//.get('/', (req, res) => {
	.get('/parramatta',  (req, res) => {
		res.render('parramatta', {
			Page: {
				Title: 'Home',
				Location: 'parramatta'
			}
		})
	})

router
	.get('/parramatta/faq',  (req, res) => {
		res.render('faq', {
			Page: {
				Title: 'FAQ',
				Location: 'parramatta/faq'
			}
		})
	})


router.get('/thank-you',  (req, res) => {
	res.render('thank-you', {
		Page: {
			Title: "Thank You",
			IsNonIndex: true,
			Map: {
				Latitude: "-33.876828",
				Longitude: "151.221028"
			}
		},
		Meta: {
			Title: "Thank You"
		}
	});
});

// Melbourne page
router
	//	remove cache.route() to disable caching.
	//.get('/', (req, res) => {
	.get('/melbourne',  (req, res) => {
		res.render('melbourne/melbourne', {
			Page: {
				Title: 'Home',
				Location: 'melbourne'
			}
		})
	})


router
	.get('/melbourne/faq',  (req, res) => {
		res.render('melbourne/faq', {
			Page: {
				Title: 'FAQ',
				Location: 'melbourne/faq'
			}
		})
	})

router
	.get('/melbourne/find-us',  (req, res) => {
		res.render('melbourne/find-us', {
			Page: {
				Title: 'Having trouble finding us?',
				Location: 'melbourne/find-us'
			}
		})
	})

module.exports = router