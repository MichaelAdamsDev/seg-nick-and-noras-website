$(document).ready(function() {

    // disable  easter dates after anzac day
    var anzacDay = moment().date(26).month(3).hour(0).minute(0).second(0)

    if (moment().isAfter(anzacDay)) $('.easter').first().hide()


    /**
     * Merchstore control
     */


    function addTotal(cost) {
        var grandTotal = $("#total").text();
        grandTotal = parseInt(grandTotal)

        grandTotal += cost;
        $("#total").text(grandTotal);
        $("#stripe-script").attr('data-amount', grandTotal);

        console.log($("#stripe-script"))
    }

    function minusTotal(cost) {
        var grandTotal = $("#total").text();
        grandTotal = parseInt(grandTotal)
        grandTotal -= cost;
        $("#total").text(grandTotal);
        $("#stripe-script").attr('data-amount', grandTotal);

        console.log($("#stripe-script"))
    }

    $('.qty-plus').on('click', function() {
        console.log('plus')
        var qtyInput = $(this).parent().find('input').first()
        var qtyDisplay = $(this).parent().find('.qty-value').first()
        console.log(qtyInput)
        console.log(qtyDisplay)
        var qty = parseInt(qtyInput.val())
        qty++;
        console.log('qty', qty)
        qtyInput.attr('value', qty)
            // qtyDisplay.attr('outerText',qty)
        qtyDisplay.text(qty)

        var cost = $(this).parent().find('.qty-cost').first()
        cost = parseInt(cost.text())
        addTotal(cost)
    });

    $('.qty-minus').on('click', function() {
        console.log('plus')
        var qtyInput = $(this).parent().find('input').first()
        var qtyDisplay = $(this).parent().find('.qty-value').first()
        console.log(qtyInput)
        console.log(qtyDisplay)
        var qty = parseInt(qtyInput.val())

        if (qty == 0) {
            return;
        }

        qty--;

        console.log('qty', qty)
        qtyInput.attr('value', qty)
            // qtyDisplay.attr('outerText',qty)
        qtyDisplay.text(qty)

        var cost = $(this).parent().find('.qty-cost').first()
        cost = parseInt(cost.text())
        minusTotal(cost)
    });


    var isMobile = ($(window).width() >= 768 ? false : true);

    $('#mobile-menu-toggle').sidr({
        name: 'menu-content',
        source: '#abs-main-nav, #abs-purchase-nav'
    });

    if (isMobile) {
        $('#menu-content a').click(function() {
            $.sidr('close', 'menu-content');
        });
        $('#menu-content [data-featherlight]').featherlight({
            afterContent: function() {
                grecaptcha.reset();
            }
        });
    };

    $('#body-blocker, #sticky-wrapper').on('touchstart', function(e) {
        e.preventDefault();
    });

    $('[data-scroll]').on('click', function(e) {

        var el = $(e.target.getAttribute('href'));
        var elOffset = el.offset().top;
        var elHeight = el.height();
        var windowHeight = $(window).height();
        var offset;

        if (elHeight < windowHeight) {
            offset = elOffset - ((windowHeight / 2) - (elHeight / 2));
        } else {
            offset = elOffset;
        }

        $.smoothScroll({
            speed: 700
        }, offset);

        return false;

    });

    // Height offset for sticky menu
    var _OFFSET = 0;

    //	Window Resize Events
    function _RESIZE() {

        //	Full Size Sections ( use .full-page )
        $('.full-page').css({
            width: $(window).width() + 'px',
            height: ((isMobile ? screen.height : $(window).height()) - _OFFSET) + 'px'
        });

        $('.three-quarter-page').css({
            width: $(window).width() + 'px',
            height: ((isMobile ? screen.height * 0.75 : $(window).height() * 0.75) - _OFFSET) + 'px'
        });

        $('.full-width').css({
            width: $(window).width() + 'px'
        });

        if (map != undefined) {

            google.maps.event.trigger(map, 'resize');

            map.setCenter(mjolner_lat_lon);

        }

    };

    //	Attach to Window Event and Run on `Ready`
    $(window).resize(_RESIZE);

    $('.about-block[data-fn]').each(function() {
        $(this).click(function() {

            switch ($(this).data('fn')) {
                case 'menu':
                    var el = $('#menu');
                    $('#menu-gallery-nav a[data-link="restaurant-food"]').first().click();
                    break;
                case 'beverages':
                    var el = $('#menu');
                    $('#menu-gallery-nav a[data-link="cocktails-and-wine"]').first().click();
                    break;
                case 'events':
                    var el = $('#events');
                    break;
            }

            var elOffset = el.offset().top;
            var elHeight = el.height();
            var windowHeight = $(window).height();
            var offset;

            if (elHeight < windowHeight) {
                offset = elOffset - ((windowHeight / 2) - (elHeight / 2));
            } else {
                offset = elOffset;
            }

            $.smoothScroll({
                speed: 700
            }, offset);

        });
    });

    var mjolner_lat_lon;
    var map;

    $('.MGLSuccessMessage').click(function() {
        $(this).remove();
    });

    var masterclassForm = null;

    // development/testing 
    // sitekey=6LfZnAkaAAAAAMeK5n_v30DTJvgE27LYmnwJUCVh

    const sitekey = '6LefHZEUAAAAAO2HdhjxZHg6Ek3-adG4vCyfPb0c';

    $('.masterclass-form-trigger').on('click', function() {
        if (!masterclassForm) {

            masterclassForm = $.featherlight('#masterclass-form', {
                persist: true,
                beforeOpen: function() {},
                afterOpen: function() {
                    grecaptcha.render('masterclass-recaptcha',
                        // Recaptcha params
                        {
                            'sitekey': '6LefHZEUAAAAAO2HdhjxZHg6Ek3-adG4vCyfPb0c',
                            'theme': 'dark',
                            callback: function(token) {
                                console.log("Verified successfully")
                                $('#masterclassFormButton').removeAttr("disabled");
                                // // Re-enable .button method required due to MGL jqueryUI include
                                $('#masterclassFormButton').button("enable");

                            },
                            'expired-callback': function() {
                                console.log("Recaptcha response: expired")
                            },
                            'error-callback': function() {
                                console.log("Recaptcha response: error")
                            },
                        },
                    );

                    $('.masterclass-datepicker').not('.hasDatePicker').datepicker({
                        dateFormat: 'dd/mm/yy',
                        minDate: 0,
                        maxDate: "+1Y",
                        onSelect: function(dateText, inst) {
                            $('input[name=DateOfYourEvent]').val(dateText);
                            $('input[name=mgldaterealval__DateOfYourEvent]').val(dateText);
                        }
                    });

                    $('.featherlight .form.contact-form').validate({
                        rules: {
                            PatronName: "required",
                            PatronSurname: "required",
                            PatronEmail: "required",
                            PatronMobile: "required",
                            DateOfYourEvent: {
                                required: true
                            },
                            'Time[]': "required",
                            EventType: "required",
                            ApproximateNumbers: "required",
                            Message: "required"
                        },
                        messages: {
                            DateOfEvent: 'Please enter a valid dd/mm/yyyy date'
                        },
                        submitHandler: function(form) {
                            var formdata = $('.featherlight form :input').serializeArray();
                            var recaptcha_response = $('#g-recaptcha-response').val();

                            $.ajax({
                                url: "/api/recaptcha",
                                type: "POST",
                                data: {
                                    formdata: JSON.stringify(formdata),
                                    recaptcha_response: recaptcha_response
                                },
                                success: function(data) {
                                    //window.location = '/thank-you';
                                    location.reload();
                                },
                                error: function(error) {
                                    console.log("Form submit error!", error);
                                    window.location = '/';
                                }
                            })
                        }
                    });
                },
                beforeClose: function() {
                    grecaptcha.reset();
                    // $('#masterclass-recaptcha').children().remove();
                }
            });
        } else {
            masterclassForm.open();
        }
    });

    // $('.contact-form').on('submit', function(e) {

    // });
    var generalForm = null;
    $('#general-enquiry-form-trigger').on('click', function() {
        if (!generalForm) {
            generalForm = $.featherlight('#general-enquiry-form', {
                persist: true,
                afterOpen: function() {
                    renderRecaptcha(sitekey, 'general-recaptcha', $(this.$instance));

                    validateAndSubmit($('.featherlight .form.contact-form'), {
                        rules: {
                            PatronName: "required",
                            PatronSurname: "required",
                            PatronEmail: "required",
                            PatronMobile: "required",
                            DateOfEvent: {
                                required: true
                            },
                            StartTimeHr: "required",
                            StartTimeMin: "required",
                            EventType: "required",
                            ApproximateNumberOfGuests: "required",
                            ApproximateBudget: "required",
                            Message: "required",
                            // gToken: {
                            // 	required:true,
                            // 	param: ''
                            // }
                        },
                        messages: {
                            DateOfEvent: 'Please enter a valid dd/mm/yyyy date'
                        }
                    })
                }
            });
        } else {
            generalForm.open();
        }
    });

    var eventForm = null;
    $('#event-enquiry-form-trigger').on('click', function() {
        if (!eventForm) {
            eventForm = $.featherlight('#event-enquiry-form', {
                persist: true,
                afterOpen: function() {
                    renderRecaptcha(sitekey, 'event-recaptcha', $(this.$instance));

                    // Initialize datepicker
                    $('.event-datepicker').not('.hasDatePicker').datepicker({
                        dateFormat: 'dd/mm/yy',
                        minDate: 0,
                        maxDate: "+1Y",
                        onSelect: function(dateText, inst) {
                            $('input[name=DateOfYourEvent]').val(dateText);
                            $('input[name=mgldaterealval__DateOfYourEvent]').val(dateText);
                        }
                    });

                    validateAndSubmit($('.featherlight .form.contact-form'), {
                        rules: {
                            PatronName: "required",
                            PatronSurname: "required",
                            PatronEmail: "required",
                            PatronMobile: "required",
                            DateOfYourEvent: {
                                required: true
                            },
                            'Time[]': "required",
                            EventType: "required",
                            ApproximateBudget: "required",
                            ApproximateNumbers: "required",
                            Message: "required"
                        },
                        messages: {
                            DateOfEvent: 'Please enter a valid dd/mm/yyyy date'
                        },
                        submitHandler: function(form) {
                            var formdata = $('.featherlight form :input').serializeArray();
                            var recaptcha_response = $('#g-recaptcha-response').val();

                            $.ajax({
                                url: "/api/recaptcha",
                                type: "POST",
                                data: {
                                    formdata: JSON.stringify(formdata),
                                    recaptcha_response: recaptcha_response
                                },
                                success: function(data) {
                                    //window.location = '/thank-you';
                                    location.reload();
                                },
                                error: function(error) {
                                    console.log("Form submit error!", error);
                                    window.location = '/';
                                }
                            })
                        }
                    })
                }
            });
        } else {
            eventForm.open();
        }
    });

    var mediaForm = null;
    $('#media-enquiry-form-trigger').on('click', function() {
        if (!mediaForm) {
            mediaForm = $.featherlight('#media-enquiry-form', {
                persist: true,
                afterOpen: function() {
                    renderRecaptcha(sitekey, 'media-recaptcha', $(this.$instance));

                    // Initialize datepicker
                    $('.media-datepicker').not('.hasDatePicker').datepicker({
                        dateFormat: 'dd/mm/yy',
                        minDate: 0,
                        maxDate: "+1Y",
                        onSelect: function(dateText, inst) {
                            $('input[name=DateOfYourEvent]').val(dateText);
                            $('input[name=mgldaterealval__DateOfYourEvent]').val(dateText);
                        }
                    });

                    validateAndSubmit($('.featherlight .form.contact-form'), {
                        rules: {
                            PatronName: "required",
                            PatronSurname: "required",
                            PatronEmail: "required",
                            PatronMobile: "required",
                            Publication: {
                                required: true
                            }
                        },
                        messages: {
                            DateOfEvent: 'Please enter a valid dd/mm/yyyy date'
                        },
                        submitHandler: function(form) {
                            var formdata = $('.featherlight form :input').serializeArray();
                            var recaptcha_response = $('#g-recaptcha-response').val();

                            $.ajax({
                                url: "/api/recaptcha",
                                type: "POST",
                                data: {
                                    formdata: JSON.stringify(formdata),
                                    recaptcha_response: recaptcha_response
                                },
                                success: function(data) {
                                    //window.location = '/thank-you';
                                    location.reload();
                                },
                                error: function(error) {
                                    console.log("Form submit error!", error);
                                    window.location = '/';
                                }
                            })
                        }
                    })
                }
            });
        } else {
            mediaForm.open();
        }
    });

    var subscribeForm = null;
    $('#subscribe-form-trigger').on('click', function() {
        if (!subscribeForm) {
            subscribeForm = $.featherlight('#subscribe-form', {
                persist: true,
                afterOpen: function() {
                    renderRecaptcha(sitekey, 'subscribe-recaptcha', $(this.$instance));

                    // Initialize datepicker
                    $('.subscribe-datepicker').not('.hasDatePicker').datepicker({
                        dateFormat: 'dd/mm/yy',
                        minDate: 0,
                        maxDate: "+1Y",
                        onSelect: function(dateText, inst) {
                            $('input[name=DateOfYourEvent]').val(dateText);
                            $('input[name=mgldaterealval__DateOfYourEvent]').val(dateText);
                        }
                    });

                    validateAndSubmit($('.featherlight .form.contact-form'), {
                        rules: {
                            PatronName: "required",
                            PatronSurname: "required",
                            PatronEmail: "required",
                            PatronMobile: "required",
                            Publication: {
                                required: true
                            }
                        },
                        messages: {
                            DateOfEvent: 'Please enter a valid dd/mm/yyyy date'
                        },
                        submitHandler: function(form) {
                            var formdata = $('.featherlight form :input').serializeArray();
                            var recaptcha_response = $('#g-recaptcha-response').val();

                            $.ajax({
                                url: "/api/recaptcha",
                                type: "POST",
                                data: {
                                    formdata: JSON.stringify(formdata),
                                    recaptcha_response: recaptcha_response
                                },
                                success: function(data) {
                                    //window.location = '/thank-you';
                                    location.reload();
                                },
                                error: function(error) {
                                    console.log("Form submit error!", error);
                                    window.location = '/';
                                }
                            })
                        }
                    })
                }
            });
        } else {
            subscribeForm.open();
        }
    });

    var subscribeForm = null;
    $('#sidr-id-subscribe-form-trigger-nav').on('click', function() {
        if (!subscribeForm) {
            subscribeForm = $.featherlight('#subscribe-form', {
                persist: true,
                afterOpen: function() {
                    renderRecaptcha(sitekey, 'subscribe-recaptcha', $(this.$instance));

                    // Initialize datepicker
                    $('.subscribe-datepicker').not('.hasDatePicker').datepicker({
                        dateFormat: 'dd/mm/yy',
                        minDate: 0,
                        maxDate: "+1Y",
                        onSelect: function(dateText, inst) {
                            $('input[name=DateOfYourEvent]').val(dateText);
                            $('input[name=mgldaterealval__DateOfYourEvent]').val(dateText);
                        }
                    });

                    validateAndSubmit($('.featherlight .form.contact-form'), {
                        rules: {
                            PatronName: "required",
                            PatronSurname: "required",
                            PatronEmail: "required",
                            PatronMobile: "required",
                            Publication: {
                                required: true
                            }
                        },
                        messages: {
                            DateOfEvent: 'Please enter a valid dd/mm/yyyy date'
                        },
                        submitHandler: function(form) {
                            var formdata = $('.featherlight form :input').serializeArray();
                            var recaptcha_response = $('#g-recaptcha-response').val();

                            $.ajax({
                                url: "/api/recaptcha",
                                type: "POST",
                                data: {
                                    formdata: JSON.stringify(formdata),
                                    recaptcha_response: recaptcha_response
                                },
                                success: function(data) {
                                    //window.location = '/thank-you';
                                    location.reload();
                                },
                                error: function(error) {
                                    console.log("Form submit error!", error);
                                    window.location = '/';
                                }
                            })
                        }
                    })
                }
            });
        } else {
            subscribeForm.open();
        }
    });

    var subscribeForm = null;
    $('#subscribe-form-trigger-nav').on('click', function() {
        if (!subscribeForm) {
            subscribeForm = $.featherlight('#subscribe-form', {
                persist: true,
                afterOpen: function() {
                    renderRecaptcha(sitekey, 'subscribe-recaptcha', $(this.$instance));

                    // Initialize datepicker
                    $('.subscribe-datepicker').not('.hasDatePicker').datepicker({
                        dateFormat: 'dd/mm/yy',
                        minDate: 0,
                        maxDate: "+1Y",
                        onSelect: function(dateText, inst) {
                            $('input[name=DateOfYourEvent]').val(dateText);
                            $('input[name=mgldaterealval__DateOfYourEvent]').val(dateText);
                        }
                    });

                    validateAndSubmit($('.featherlight .form.contact-form'), {
                        rules: {
                            PatronName: "required",
                            PatronSurname: "required",
                            PatronEmail: "required",
                            PatronMobile: "required",
                            Publication: {
                                required: true
                            }
                        },
                        messages: {
                            DateOfEvent: 'Please enter a valid dd/mm/yyyy date'
                        },
                        submitHandler: function(form) {
                            var formdata = $('.featherlight form :input').serializeArray();
                            var recaptcha_response = $('#g-recaptcha-response').val();

                            $.ajax({
                                url: "/api/recaptcha",
                                type: "POST",
                                data: {
                                    formdata: JSON.stringify(formdata),
                                    recaptcha_response: recaptcha_response
                                },
                                success: function(data) {
                                    //window.location = '/thank-you';
                                    location.reload();
                                },
                                error: function(error) {
                                    console.log("Form submit error!", error);
                                    window.location = '/';
                                }
                            })
                        }
                    })
                }
            });
        } else {
            subscribeForm.open();
        }
    });


    $.imgpreload(['/img/nicknora.png', '/img/nr-logo.png'], {
        all: function() {

            $('body').addClass('core-ready');
            $('#global-page-loader').remove();

            //	Floating/Sticky Navigation Bar
            $('.float-nav').sticky({
                topSpacing: 0
            });

            if (!isMobile) {

                //	Reveal Setup
                window.sr = ScrollReveal({
                    duration: 1000,
                    //distance:0,
                    scale: 1.05,
                    mobile: true,
                    reset: false
                });

                //	-	About Section
                sr.reveal('.logo-player');
                sr.reveal('.about-header');
                sr.reveal('.about-content');
                sr.reveal('.about-block');
                sr.reveal('.bookings-wrapper .container');
                sr.reveal('.contact-container');
                sr.reveal('.bookings-form');
                sr.reveal('.container-wallpaper');

            } else {

                $('.gift-vouchers').remove();

            }

            $('#menu').removeClass('loading');

            var PageCurlDefaults = {
                height: 670,
                width: 944,
                flipSound: false,
                centeredWhenClosed: true,
                hardcovers: true,
                responsiveHandleWidth: 50,
                container: "#menu-gallery",
                containerPadding: "5px",
                pageNumbers: false,
                turnPageDuration: 300,
                bookShadow: false,
                shadows: true,
                deepLinking: false,
                updateBrowserURL: false,
                zoomBoundingBox: $('#menu')
            };

            //	Menu Gallery
            var MenuSlider = $("#menu-gallery").lightSlider({
                item: 1,
                autoWidth: false,
                slideMargin: 0,

                addClass: '',
                mode: "slide",
                useCSS: true,
                cssEasing: 'ease', //'cubic-bezier(0.25, 0, 0.25, 1)',//
                easing: 'linear',

                speed: 400, //ms'
                auto: false,
                loop: false,
                slideEndAnimation: true,
                pause: 2000,

                keyPress: false,
                controls: false,
                prevHtml: '',
                nextHtml: '',

                adaptiveHeight: false,

                pager: false,
                gallery: false,

                enableTouch: false,
                enableDrag: false,
                freeMove: false,

                //responsive : [],

                onBeforeStart: function(el) {
                    //kill pagecurl
                },
                onSliderLoad: function(el) {},
                onBeforeSlide: function(el) {
                    $('#menu-gallery-nav a.active').removeClass('active');
                },
                onAfterSlide: function(el) {
                    $($('#menu-gallery-nav a')[MenuSlider.getCurrentSlideCount() - 1]).addClass('active').removeClass('clicked');
                    //start pagecurl
                },
                onBeforeNextSlide: function(el) {},
                onBeforePrevSlide: function(el) {}
            });

            //  Menu Page Curls
            document.Books = {
                'RawBar': $('#book-raw-bar').wowBook($.extend({
                    controls: {
                        next: '#book-raw-bar-next',
                        back: '#book-raw-bar-prev'
                    }
                }, PageCurlDefaults)),
                'Cocktails': $('#book-cocktails').wowBook($.extend({
                    controls: {
                        next: '#book-cocktails-next',
                        back: '#book-cocktails-prev'
                    }
                }, PageCurlDefaults)),
                'Champagne': $('#book-champagne').wowBook($.extend({
                    controls: {
                        next: '#book-champagne-next',
                        back: '#book-champagne-prev'
                    }
                }, PageCurlDefaults)),
                'WineBeer': $('#book-wine-beer').wowBook($.extend({
                    controls: {
                        next: '#book-wine-beer-next',
                        back: '#book-wine-beer-prev'
                    }
                }, PageCurlDefaults)),
                'Cigars': $('#book-cigars').wowBook($.extend({
                    controls: {
                        next: '#book-cigars-next',
                        back: '#book-cigars-prev'
                    }
                }, PageCurlDefaults)),
                'Test': $('#book-test').wowBook($.extend({
                    controls: {
                        next: '#book-test-next',
                        back: '#book-test-prev'
                    }
                }, PageCurlDefaults)),
                'EventsPackage': $('#book-events').wowBook($.extend({
                    controls: {
                        next: '#book-events-next',
                        back: '#book-events-prev'
                    },
                    gutterShadow: false
                }, PageCurlDefaults)),
                'Booze': $('#book-booze').wowBook($.extend({
                    controls: {
                        next: '#book-booze-next',
                        back: '#book-booze-prev'
                    },
                    gutterShadow: false
                }, PageCurlDefaults))
            };

            //  Attach Menu Gallery Nav to Slider
            $('#menu-gallery-nav a').each(function(num, elem) {
                $(this).click(function() {
                    $(this).addClass('clicked');
                    MenuSlider.goToSlide((num));
                    var boof_ref = $(this).data('book-ref');
                    $.wowBook(document.Books[boof_ref]).showPage(1, true);
                })
            });

            $.wowBook(document.Books.EventsPackage).showPage(1, true);

            _RESIZE();

        }
    });

    $('#events-feed').empty().addClass('loading');

    // $.getJSON('/api/events', function (data) {

    // 	data.forEach(function (evt, intr) {
    // 		var event_row = $('<a>').attr({
    // 			class: 'event-feed-item embers',
    // 			target: '_blank',
    // 			href: evt.source,
    // 			itemscope: null,
    // 			itemtype: "http://schema.org/Event",
    // 			itemprop: "url"
    // 		});
    // 		event_row.append($('<img>').attr({
    // 			itemprop: "image",
    // 			src: (evt.image.source),
    // 			class: 'img-responsive'
    // 		}));
    // 		var content_block = $('<div>').addClass('event-content');
    // 		content_block.append($('<h2>').attr({
    // 			itemprop: "name"
    // 		}).html(evt.name));
    // 		// Requested remove StartDate
    // 		// content_block.append($('<h3>').attr({
    // 		// 	itemprop: "startDate",
    // 		// 	content: evt.time.start
    // 		// }).html(moment(evt.time.start).fromNow()));
    // 		content_block.append($('<p>').html(evt.description));
    // 		content_block.append($('<div>').attr({
    // 			class: 'about-block-link'
    // 		}).append($('<div>').attr({
    // 			class: 'button ember'
    // 		}).html('More Info')));
    // 		event_row.append(content_block);
    // 		var block_size = '';
    // 		switch (data.length) {
    // 			case 1:
    // 				block_size = 'col-sm-6 col-sm-offset-3';
    // 				break;
    // 			case 2:
    // 				block_size = 'col-sm-6';
    // 				break;
    // 			default:
    // 				block_size = 'col-sm-4';
    // 				break;
    // 		}
    // 		var event_block = $('<div>').attr({
    // 			class: (block_size)
    // 		});
    // 		event_block.append(event_row);
    // 		$('#events-feed').append(event_block);
    // 	});

    // 	if (data.length == 0) {
    // 		$('#events-feed').append($('<div>').addClass('no-events-notifier').html('No Upcoming Events, Check Back Soon!'));
    // 	}

    // 	$('#events-feed').removeClass('loading');

    // });

    _RESIZE();

    // $('#mf591d47ed75f36_DateOfEvent').datepicker('destroy');

    // $('#mf591d47ed75f36_DateOfEvent').datepicker({
    // 	dateFormat:'dd/mm/yy',
    // 	minDate: '0',
    // 	yearRange: '2017:2027',
    // 	onSelect: function(dateText, inst) {
    // 		console.log("test");
    // 		validatorContactForm.element('#mf591d47ed75f36_DateOfEvent');
    // 	}
    // });

    // var validatorContactForm = $('#mf591d47ed75f36').validate({
    // 	rules: {
    // 		PatronName: "required",
    // 		PatronSurname: "required",
    // 		PatronEmail: "required",
    // 		PatronMobile: "required",
    // 		DateOfEvent: {
    // 			required:true
    // 		},
    // 		StartTimeHr: "required",
    // 		StartTimeMin: "required",
    // 		EventType: "required",
    // 		ApproximateNumberOfGuests: "required",
    // 		ApproximateBudget: "required",
    // 		Message: "required"
    // 	},
    // 	messages:{
    // 		DateOfEvent:'Please enter a valid dd/mm/yyyy date'
    // 	}
    // });

    $('#mf58b8ddf83afcf').validate({
        rules: {
            PatronName: "required",
            PatronSurname: "required",
            PatronEmail: "required",
            PatronMobile: "required",
        }
    });

    //data-ga-event="CATEGORY:ACTION:LABEL:VALUE"
    $('[data-ga-event]').on('click', function() {
        console.log("Event tracked");
        let Opts = $(this).data('ga-event').split(':');
        if (Opts.length != 4) return;
        ga('send', 'event', Opts[0], Opts[1], Opts[2], Opts[3]);
    });

    $('input[name="contact_via_fax"').css('display', 'none');

    function disableEnter() {

        $("form").each(function() {

            $(this).keypress(function(e) {
                //Enter key
                if (e.which == 13) {
                    return false;
                }
            });

        })



    }

    // Render Google Recaptcha
    function renderRecaptcha(sitekey, recaptchaContainer, self) {
        grecaptcha.render(recaptchaContainer,
            // Recaptcha params
            {
                'sitekey': sitekey,
                'theme': 'dark',
                callback: function(token) {
                    console.log("Verified successfully")
                    self.find('.formButton').removeAttr("disabled");
                    // Re-enable .button method required due to MGL jqueryUI include
                    self.find('.formButton').button("enable");

                },
                'expired-callback': function() {
                    console.log("Recaptcha response: expired")
                },
                'error-callback': function() {
                    console.log("Recaptcha response: error")
                },
            });
    }

    // Validate and submit form
    function validateAndSubmit($form, validateOptions) {
        $form.validate({
            validateOptions,
            submitHandler: function($form) {
                var formdata = $('.featherlight form :input').serializeArray();
                var recaptcha_response = $('#g-recaptcha-response').val();

                $.ajax({
                    url: "/api/recaptcha",
                    type: "POST",
                    data: {
                        formdata: JSON.stringify(formdata),
                        recaptcha_response: recaptcha_response
                    },
                    success: function(data) {
                        console.log("Form submit success!", data);
                        //window.location = '/thank-you';
                        location.reload();
                    },
                    error: function(error) {
                        console.log("Form submit error!", error);
                        window.location = '/';
                    }
                });
            }
        });
    }
    function socialIcons() {
        $(".sidr-class-fa.sidr-class-fa-instagram").addClass("fa fa-instagram");
        $(".sidr-class-fa.sidr-class-fa-facebook").addClass("fa fa-facebook");    
    }
    socialIcons()

    // Hash trigger for speakeasygroup.com.au/career
    if(window.location.hash == "#join-our-team") {
        $.featherlight("#join-our-team-content");
    }
    
    // Hash trigger for speakeasygroup.com.au/exclusive-venue
    if(window.location.hash == "#exclusive-venue") {
        $.featherlight("#event-enquiry-form");
    }

});

var url
var token
var baseUrl = 'myguestlist.com/mgl/formreceiver.php'

function gotToken(response) {
    console.log('responseresponseresponse', response)
    token = response
}



function objectifyForm(formArray) { //serialize data function

    var returnArray = {};
    for (var i = 0; i < formArray.length; i++) {
        returnArray[formArray[i]['name']] = formArray[i]['value'];
    }
    return returnArray;
}

function postForm(form) {

    console.log('before post', form)

    returnArray;

    $.ajax({
        type: "POST",
        url: 'https://hooks.zapier.com/hooks/catch/2091030/weixe5',
        dataType: 'json',
        async: true,
        data: payload,
        success: function() {
            $('#submittedModal').modal('show');
        }
    })
}