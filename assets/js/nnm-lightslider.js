$(document).ready(function(){
	
	// On ready init slider
	var masterClassSlides = $('#masterClassSlider').lightSlider({
	  item: 4,
		slideMove: 1,
		autoWidth: false,
		slideMargin: 0,

		addClass: 'light-slider',
		mode: "slide",
		useCSS: true,
		cssEasing: 'ease', //'cubic-bezier(0.25, 0, 0.25, 1)',//
		easing: 'linear',

		speed: 400, //ms'
		auto: false,
		loop: false,
		slideEndAnimation: true,
		pause: 2000,

		keyPress: false,
		controls: true,
		prevHtml: '',
		nextHtml: '',

		adaptiveHeight: false,

		pager: false,
		gallery: false,

		enableTouch: false,
		enableDrag: false,
		freeMove: false,
		responsive: [
			{
				breakpoint: 1200,
					settings: {
						item: 3,
						slideMove: 1
				}
			},
			{
				breakpoint: 1040,
					settings: {
						item: 2,
						slideMove: 1
				}
			},
			{
				breakpoint: 578,
					settings: {
						item: 1,
						slideMove: 1
				}
			},
		],
		onSliderLoad: function() {
				$( window ).resize();
		}, onAfterSlide: function() {
				$( window ).resize();
		}
	});
	setTimeout(function () {

			try {
				masterClassSlides.refresh();    
			} catch (error) {
					console.log('error in slider', error)
			}
	}, 1000);

})