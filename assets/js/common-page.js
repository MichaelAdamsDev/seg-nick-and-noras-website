$(document).ready(function(){

	var isMobile = ($(window).width()>=768?false:true);

	$('#mobile-menu-toggle').sidr({
	    name: 'menu-content',
	    source: '#abs-main-nav, #abs-purchase-nav'
    });

    $('#body-blocker, #sticky-wrapper').on('touchstart',function(e){
    	e.preventDefault();
    });

	$('[data-scroll]').on('click', function(e) { 

		var el = $( e.target.getAttribute('href') );
		var elOffset = el.offset().top;
		var elHeight = el.height();
		var windowHeight = $(window).height();
		var offset;

		if (elHeight < windowHeight) {
			offset = elOffset - ((windowHeight / 2) - (elHeight / 2));
		} else {
			offset = elOffset;
		}

		$.smoothScroll({ speed: 700 }, offset);

		return false;

	});

	// Height offset for sticky menu
	var _OFFSET = 0;

	//	Window Resize Events
	function _RESIZE(){

		//	Full Size Sections ( use .full-page )
		$('.full-page').css({
			width:$(window).width()+'px',
			height:((isMobile?screen.height:$(window).height())-_OFFSET)+'px'
		});

		$('.three-quarter-page').css({
			width:$(window).width()+'px',
			height:((isMobile?screen.height*0.75:$(window).height()*0.75)-_OFFSET)+'px'
		});

		$('.full-width').css({
			width:$(window).width()+'px'
		});

		if(map != undefined){

			google.maps.event.trigger(map, 'resize');

			map.setCenter(mjolner_lat_lon);

		}

	};

	//	Attach to Window Event and Run on `Ready`
	$(window).resize(_RESIZE);

	//	Ember Sparks
	$('.embers').click(function(){
		var _ember = $('<div>').addClass('ember-spark');
			_off = $(this).offset(),
			xPos = event.pageX - _off.left,
			yPos = event.pageY - _off.top;
		_ember.css({
			height:($(this).height()),
			width:($(this).height()),
			left:xPos,
			top:yPos,
			margin:('-'+($(this).height()*0.5)+'px 0 0 -'+($(this).height()*0.5)+'px')
		});
		$(this).append(_ember);
		setTimeout(function(){
			_ember.remove();
		},1000);
	});

	var mjolner_lat_lon;
	var map;

	$('.MGLSuccessMessage').click(function(){
		$(this).remove();
	});

	//	Floating/Sticky Navigation Bar
	$('.float-nav').sticky({
		bottomSpacing:0
	});

	$.imgpreload(['/img/mjolner-logo.png','/img/contact-background.jpg'],{
		all: function(){

			$('body').addClass('core-ready');
			$('#global-page-loader').remove();

			//Google Maps
			// function generate_map() {
				
			// 		mjolner_lat_lon = new google.maps.LatLng(-37.8121341,144.9584858,17);

			// 		// Basic options for a simple Google Map
			// 		// For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
			// 		var mapOptions = {
			// 		// How zoomed in you want the map to start at (always required)
			// 		zoom: 15,
			// 		scrollwheel: false,
			// 		draggable: false,
					
			// 		// The latitude and longitude to center the map (always required)
			// 		center: mjolner_lat_lon,
					
			// 		// How you would like to style the map. 
			// 		// This is where you would paste any style found on Snazzy Maps.
							
			// 		styles: [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#000000"},{"lightness":40}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#000000"},{"lightness":16}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#5a5b60"}]},{"featureType":"administrative","elementType":"labels.text.stroke","stylers":[{"visibility":"off"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"lightness":20},{"color":"#212328"}]},{"featureType":"landscape.man_made","elementType":"geometry.stroke","stylers":[{"visibility":"on"},{"color":"#35373c"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#27282d"}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"color":"#2d2e33"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#5a5b60"}]},{"featureType":"road","elementType":"labels.text.stroke","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"road.arterial","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"road.local","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#35373c"}]}]

			// 	};
				
			// 	map = new google.maps.Map(document.getElementById('map'), mapOptions);

			// 	// Set the text contained in the bubble; Let this part well in one line, no newline.
			// 	var contentString = '<div class="info-content">267 Cleveland St, Redfern NSW 2016</small></h2></div>';

			// 	var infowindow = new google.maps.InfoWindow({
			// 		content: contentString
			// 	});

			// 	var marker_url = 'https://mjolner.com.au/img/marker.png';
				
			// 	var marker = new google.maps.Marker({
			// 		position: mjolner_lat_lon,
			// 		map: map,
			// 		title: 'More informations',
			// 		icon: marker_url
			// 	});
				
			// 	google.maps.event.addListener(marker, 'click', function() {
			// 		window.location.href="https://www.google.com.au/maps/place/Mj%C3%B8lner/@-33.8900675,151.2051122,17z/data=!3m1!4b1!4m5!3m4!1s0x6b12b1df0aa5d4c7:0x64fb2243ee2ccc98!8m2!3d-33.890072!4d151.2073062";
			// 	});
				
			// };

		}
	});

	_RESIZE();

})