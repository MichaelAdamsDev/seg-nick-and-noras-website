/*
*	#	Express JS Boilerplate
*/

const express = require('express');

const app = express();

const fs = require('fs');

const merge = require('merge');

express.__dir = __dirname; // For "Root Dir" access in sub-routes.

const mail = require('@sendgrid/mail');

const options = require('config-yml');

const redisService = require('redis');

const redis = redisService.createClient({ prefix: options.RedisPrefix });

const Raven = require('raven');

Raven.config(options.Sentry.Private).install();

mail.setApiKey(options.SendGrid.APIKey);

app.set('view engine', 'pug');

app.set('view options', { basedir: process.env.__dirname});

app.locals.basedir = app.get('views');

const favicon = require('serve-favicon');

const path = require('path');

app.use(favicon(path.join(__dirname, 'assets', 'favicon.ico')));

const bodyParser = require('body-parser');

app.use(require('send-seekable'))

app.use( bodyParser.json() );

app.use( bodyParser.urlencoded({ extended: true }) ); 

//	If Dev initiate morgan logging

if(options.Environment === 'dev') app.use(require('morgan')('dev'));

const Datastore = require('nedb');

const enquiries = new Datastore({ filename: options.Datastore+'enquiries.db' });

//	Set static assets route
app
	.use(express.static('assets'));

//	Define view object defaults.
app
	.locals = merge(app.locals,options.Locals);

//	Append Public Sentry Token
app
	.locals = merge(app.locals,{SentryToken:options.Sentry.Public});

//	Automatic routing middleware

const resolve = require('app-root-path').resolve;

function LoadRoutes(app, target) {

	if (!(this instanceof LoadRoutes))
		return new LoadRoutes(app, target);

	this.Construct(app, target);

}

LoadRoutes.prototype.Construct = function (app, target) {

	target = resolve(typeof target === 'string' ? target : 'routes');
	
	this.ScanFolder(target).forEach((file) => {
	
		var route, router;
		
		route  = this.pathToRoute(file, target);
		
		router = require(file);
		
		if (typeof router !== 'function') return;
		
		app.use(route, router);
	
	}, this);

};

LoadRoutes.prototype.ScanFolder = function (target) {

	var files = [];

	if (typeof target !== 'string')
		throw new TypeError('Expecting target path to be a string');

	if (target.charAt(0) === '.')
		// resolve the target path
		target = path.resolve(path.dirname(module.parent.filename), target);

	// return an empty array if target does not exists
	if (!fs.existsSync(target)) return files;

	// look for files recursively
	fs.readdirSync(target).forEach((file) => {

		let filePath = path.join(target, file);

		if (fs.lstatSync(filePath).isFile()){
			if( path.extname(file) == '.js' )
				files.push(filePath);
		}else{
			files.push.apply(files, this.ScanFolder(filePath));
		}

	}, this);

	return files;

};

LoadRoutes.prototype.pathToRoute = function (target, base) {

	// remove file extension and normalize slashes
	target = path.normalize(target);
	target = target.replace(path.extname(target), '');

	if (base && typeof base === 'string') {

		var segments = [], segment;

		if (process.platform === 'win32') {
			target = target.split("\\");
			base = path.normalize(base).split("\\");
		} else {
			target = target.split('/');
			base = path.normalize(base).split('/');
		}
		
		base   = base[base.length - 1];

		for (var i = target.length - 1; i >= 0; i--) {
			segment = target[i];
			if (segment === base) break;
			if (i === target.length - 1 && segment === 'index') continue;
			if (segment !== '') segments.push(segment);
		}

		return '/' + segments.reverse().join('/');

	}

	// without a base, use the last segment
	target = path.basename(target);

	return '/' + (target !== 'index' ? target : '');

};

LoadRoutes(app, '/routes');

/*
*	NOTE: This is not suitable for a multi site VPS
*/

console.log('Clearing In-Memory Page Cache');

redis.flushall( (err, succeeded) => {

	console.log('Done');
 
	app.listen(options.Port,(res)=>{

		console.log('Listening :'+options.Port);

		// if (options.Environment == 'dev') {

		// 	console.log('Opening localhost:'+options.Port+' in Browser');
			
		// 	const opn = require('opn');
		// 	opn('http://localhost:' + options.Port, {
		// 	app: ['google chrome', '--incognito']
		// 	});

		// }
	
	});

});

module.exports = app;