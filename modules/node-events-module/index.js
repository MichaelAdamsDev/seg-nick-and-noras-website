var redis = require("redis");
const options = require('config-yml');
var redisClient = redis.createClient({ prefix: options.RedisPrefix });
var facebookClient = require("./facebook");

function EventClient(config){
    this.__proto__ = EventClient.prototype;
    this.config = config;
    this.clients = {
        facebook: new facebookClient.FacebookClient(config)
    };
    return this;
}

EventClient.prototype.getEvents = function(callbackFunction){
        var scope = this;
        redisClient.get(this.config.generalDetails.redisCacheKeys.dataCache, function(err, generalCache){
            if(err) //error getting token from redis, log to console then callback
            {
                console.error("Error getting token from Redis:");
                console.error(err);
                callbackFunction(err, null);
                return;
            }
            if(generalCache == null || generalCache == "undefined"){
                    var events = [];
                    scope.clients.facebook.getEvents(function(err, fbData){
                        if(err){
                            console.error(err); // failed to get facebook event data, console log and error
                            callbackFunction(err, null);
                            return;
                        }
                        fbData.data.forEach(function(event){
                            events.push(scope.clients.facebook.normalize(event))
                        })
                        redisClient.expire(scope.config.generalDetails.redisCacheKeys.dataCache, scope.config.generalDetails.autoExpireTime);
                        callbackFunction(null, events);
                        return;
                    });
                    return;
            }
            callbackFunction(null, JSON.parse(generalCache));
        });
    };

EventClient.prototype.clearCache = function(callback){
    var scope = this;
    redisClient.del(scope.config.facebookAppDetails.redisCacheKeys.accessToken, function(err, data){
        if(err){
            console.error(err); // failed to delete facebook event data, console log and error
            callback(err, null);
            return;
        }
        redisClient.del(scope.config.facebookAppDetails.redisCacheKeys.dataCache, function(err, data){
            if(err){
                console.error(err); // failed to delete facebook event data, console log and error
                callback(err, null);
                return;
            }
            redisClient.del(scope.config.generalDetails.redisCacheKeys.dataCache, function(err, data){
                if(err){
                    console.error(err); // failed to delete general (normaized) event data, console log and error
                    callback(err, null);
                    return;
                }
                callback(null, true);
            });
        });
    });
};

exports.EventClient = EventClient;