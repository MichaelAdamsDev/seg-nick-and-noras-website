var FB = require('fb');
var redis = require("redis");
const options = require('config-yml');
var redisClient = redis.createClient({ prefix: options.RedisPrefix });

function FacebookClient(config){
    this.config = config;
    this.fb = new FB.Facebook({version: config.facebookAppDetails.apiVersion});
    this.getEvents = FacebookClient.prototype.getEvents;
    this.getOrUpdateToken = FacebookClient.prototype.getOrUpdateToken;   
    this.normalize = FacebookClient.prototype.normalize
}

FacebookClient.prototype.getOrUpdateToken = function(callback, refresh = false) {
    var scope = this;
    // redisClient.get(this.config.facebookAppDetails.redisCacheKeys.accessToken, function(err, accessToken){
    //     if(err) //error getting token from redis, log to console then callback
    //     {
    //         console.error("Error getting token from Redis:");
    //         console.error(err);
    //         callback(err, null);
    //         return;
    //     }
    //     if(accessToken == null || refresh || accessToken == "undefined"){ // no error getting token, and token is null: get facebook token and save
    //         scope.fb.api("oauth/access_token", 
    //             { 
    //                 client_id: scope.config.facebookAppDetails.appId, 
    //                 client_secret: scope.config.facebookAppDetails.appSecret, 
    //                 grant_type: "client_credentials"
    //             }, function(facebookTokenResponse){
    //                 if(!facebookTokenResponse){
    //                     //request failed, log to console then callback
    //                     console.log("Failed to collect access token to facebook, using appId: " + scope.config.facebookAppDetails.appId)
    //                     callback("Failed to collect access token", null);
    //                     return;
    //                 }
    //                 //request success, save token and get event data
    //                 facebookAccessToken = facebookTokenResponse.access_token;
    //                 redisClient.set(scope.config.facebookAppDetails.redisCacheKeys.accessToken, facebookAccessToken, function(err){
    //                     if(err) {
    //                         // error setting facebookAccessToken, log then calback.
    //                         console.error(err);
    //                         callback(err, null);
    //                         return;
    //                     }
    //                     //collected facebookAccessToken, set to client and callback with token.
    //                     scope.fb.setAccessToken(facebookAccessToken);
                        
    //                     callback(null, facebookAccessToken);
    //                 });                                 
    //             });
    //             return;
    //     }
    //     //access token returned from redis
    //     callback(null, accessToken);
    // });
    callback(null, "EAAZAeLGs53u0BAKcHi57CrLhpsJeCoCW6ta6l8F7pEOs9Fa5ZAiKLoQY7xEXOZCMKO2r9a9aIyjlcgIGZB9XxGFX5cviQcliHygKbbTfDLJOi7sHChTDgaFVV0HdEVDMz6h1KQvKYDkcgYf0VJAh7gbEESw3guAZD");
};

FacebookClient.prototype.getEvents = function(callback, refresh = false){
    var scope = this;
    //Get access token
    redisClient.get(this.config.facebookAppDetails.redisCacheKeys.dataCache, function(err, facebookCache){
        if(err) //error getting token from redis, log to console then callback
        {
            console.error("Error getting token from Redis:");
            console.error(err);
            callback(err, null);
            return;
        }
        if(facebookCache == null || refresh || facebookCache == "undefined"){
            scope.getOrUpdateToken(function(err, token){
                if(err){
                    //error when getting token, log to console then callback
                    console.error(err);
                    callback(err, null);
                    return;
                }
                //set access token, get event data, then callback
                scope.fb.setAccessToken(token);            
                scope.fb.api(scope.config.facebookAppDetails.pageId + "/events", { fields: scope.config.facebookAppDetails.dataFields, time_filter: 'upcoming'},
                    function(facebookEventResponse){
                        if(!facebookEventResponse) {
                            console.error("No event data returned from facebook")
                            callback("No event data", null);
                            return;
                        }
                        if(facebookEventResponse.error){
                            console.error("Facebook request error: ", facebookEventResponse.error);
                            callback(facebookEventResponse.error, null);
                            return;
                        }

                        redisClient.set(scope.config.facebookAppDetails.redisCacheKeys.dataCache, JSON.stringify(facebookEventResponse), function(err){
                            if(err) {
                                // error setting facebookAccessToken, log then calback.
                                console.error(err);
                                callback(err, null);
                                return;
                            }
                            //set data into redis, so callback
                            redisClient.expire(scope.config.facebookAppDetails.redisCacheKeys.dataCache, scope.config.facebookAppDetails.autoExpireTime);
                            callback(null, facebookEventResponse);
                        });           

                        
                        return;
                    });
            });
            return;
        }
        callback(null, JSON.parse(facebookCache));
    });
};

FacebookClient.prototype.normalize = function(facebookEvent){
    
    var newEvent = {
        name: facebookEvent.name,
        time: {
            start: facebookEvent.start_time,
            end: facebookEvent.end_time
        },
        description: facebookEvent.description,
        facebookEvent: facebookEvent,
        source: "https://facebook.com/" + facebookEvent.id,
    };
    if(facebookEvent.cover){
        newEvent.image = 
        {
            source: facebookEvent.cover.source
        };
    }
    else
    {
        newEvent.image = {
            error: "Image not available",
            source: null
        }
    }
    return newEvent;
}

exports.FacebookClient = FacebookClient;